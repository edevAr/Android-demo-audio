package com.example.edenarielguzmanandia.audio;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {
    private LinearLayout container;

    private MediaPlayer mediaPlayer;
    private boolean play;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeProperties();
    }
    private void initializeProperties(){
        container = (LinearLayout)findViewById(R.id.container);
        mediaPlayer = MediaPlayer.create(this, R.raw.audio);
        play = false;

        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!play){
                    mediaPlayer.start();
                    play = true;
                }
                else{
                    mediaPlayer.pause();
                    play = false;
                }
            }
        });
    }
}
